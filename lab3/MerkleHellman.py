import utils
import random


class PrivateKey:
    def __init__(self, w, q, r):
        self.w = w
        self.q = q
        self.r = r


class PublicKey:
    def __init__(self, beta):
        self.beta = beta


# Atm my version of MerkleHellman encryption only works if n is set to 8, bc. the assignment said that doing it this way was acceptable
def generate_private_key(n=8):
    starterValue = random.randint(2, 10)
    sumOfValues = starterValue
    w = [starterValue]
    for i in range(n-1):
        w.append(random.randrange(sumOfValues + 1, sumOfValues * 2))
        sumOfValues += w[-1]

    q = random.randrange(sumOfValues + 1, sumOfValues * 2)

    r = random.randrange(2, q-1)
    while not utils.coprime(r, q):
        r = random.randrange(2, q-1)

    return tuple(w), q, r


def create_public_key(privKey):
    return tuple([(privKey.r * w_i) % privKey.q for w_i in privKey.w])


def encrypt_mh(message, pubKey):
    encryptedMsg = []
    for character in message:
        charBytes = utils.byte_to_bits(character.encode()[0])
        c = sum(a_i * b_i for a_i, b_i in zip(charBytes, pubKey))

        encryptedMsg.append(c)

        # print(*zip(pubKey.beta, charBytes))
        # print(c)

    return encryptedMsg


def decrypt_mh(message, privKey):
    s = utils.modinv(privKey.r, privKey.q)

    alteredMessage = [cc * s % privKey.q for cc in message]
    lenMessage = len(alteredMessage)
    lenW = len(privKey.w)

    decryptedMsg = ''

    for i in range(lenMessage):
        actPosition = lenMessage - (i+1)
        alpha = [0 for _ in range(8)]

        for j in range(lenW):
            actPosW = lenW - (j+1)
            if alteredMessage[actPosition] >= privKey.w[actPosW]:
                alpha[j] = 1
            else:
                alpha[j] = 0

            alteredMessage[actPosition] -= privKey.w[actPosW] * alpha[j]

        c = chr(
            sum([2 ** pwr for a_i, pwr in zip(alpha, range(len(alpha))) if a_i == 1]))

        decryptedMsg += c

    return decryptedMsg[::-1]

# * -> unpacks a tuple
# privKey = PrivateKey(*generate_private_key())
# pubKey = PublicKey(create_public_key(privKey))

# encryptedMsg = encrypt_mh("<3 StanfordPython", pubKey)

# print(decrypt_mh(encryptedMsg, privKey))
