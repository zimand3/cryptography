class BBS:
    p = 4271
    q = 7043
    seed = 0
    n = 0
    next = 0

    def __init__(self, seed):
        self.seed = seed
        self.n = self.p * self.q
        self.next = (seed ** 2) % self.n

    def generate(self, n):
        if n < 0:
            return []

        result = []

        for i in range(n):
            byte = []
            for j in range(8):
                byte.append(self.next % 2)
                self.next = (self.next ** 2) % self.n
            result.append(int(''.join(map(str. byte)), 2))
        return bytes(result)

    def generate(self, low, high):
        if high < low:
            return []

        if low < 0:
            return []

        result = []
        self.next = (self.seed ** 2) % self.n

        for i in range(low):
            for j in range(8):
                self.next = (self.next ** 2) % self.n

        for i in range(i, j):
            byte = []
            for j in range(8):
                byte.append(self.next % 2)
                self.next = (self.next ** 2) % self.n

            result.append(int(''.join(map(str. byte)), 2))

        return bytes(result)
