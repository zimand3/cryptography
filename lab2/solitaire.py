class Solitaire:
    startingDeck = []
    deck = []
    joker_A = 0
    joker_B = 0

    def __init__(self, deck) -> None:
        self.startingDeck = deck.copy()
        self.deck = deck.copy()
        self.joker_A = self.deck.index(53)
        self.joker_B = self.deck.index(54)

        for i in range(1, 55):
            if i not in deck:
                raise ValueError("Invalid deck")

        if len(deck) != 54:
            raise ValueError("Invalid deck")

    def setJokers(self):
        self.joker_A = self.deck.index(53)
        self.joker_B = self.deck.index(54)

    def moveJokers(self):
        self.setJokers()
        self.deck.pop(self.joker_A)
        if self.joker_A < 53:
            self.deck.insert(self.joker_A + 1, 53)
            self.joker_A = self.joker_A + 1
        else:
            self.deck.insert((self.joker_A + 2) % 54, 53)
            self.joker_A = (self.joker_A + 2) % 54

        self.setJokers()
        self.deck.pop(self.joker_B)
        if self.joker_B < 52:
            self.deck.insert(self.joker_B + 2, 54)
            self.joker_B = self.joker_B + 2
        else:
            self.deck.insert((self.joker_B + 3) % 54, 54)
            self.joker_B = (self.joker_B + 3) % 54
        self.setJokers()

    def jokerCut(self):
        first = min(self.joker_A, self.joker_B)
        second = max(self.joker_A, self.joker_B)

        self.deck = self.deck[second + 1:] + \
            self.deck[first:second + 1] + self.deck[:first]

        self.setJokers()

    def countCut(self):
        count = self.deck[-1]

        if count < 53:
            self.deck = self.deck[count + 1: -1] + \
                self.deck[:count + 1] + self.deck[-1:]
            self.setJokers()

    def getKey(self):
        count = self.deck[0]

        if count < 53:
            return self.deck[count + 1]
        else:
            return -1

    def generate(self, n):
        if n < 0:
            return []

        result = []

        for i in range(n):
            byte = []
            j = 0
            while j < 8:
                self.moveJokers()
                self.jokerCut()
                self.countCut()
                keyCard = self.getKey()

                if keyCard != -1:
                    byte.append(keyCard % 2)
                    j += 1

                result.append(int(''.join(map(str, byte)), 2))

            return bytes(result)

    def generateWO(self, low, high):
        if high < low:
            return []

        if low < 0:
            return []

        result = []

        self.deck = self.startingDeck.copy()
        self.setJokers()

        for i in range(low):
            y = 0
            while y < 8:
                self.moveJokers()
                self.jokerCut()
                self.countCut()
                keyCard = self.getKey()
                if keyCard != -1:
                    y += 1

        for i in range(low, high):
            byte = []
            y = 0
            while y < 8:
                self.moveJokers()
                self.jokerCut()
                self.countCut()
                keyCard = self.getKey()
                if keyCard != -1:
                    byte.append(keyCard % 2)
                    y += 1

            result.append(int(''.join(map(str. byte)), 2))
        return bytes(result)
