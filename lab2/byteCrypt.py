class byteCrypt:

    cryptModule = None
    key = None
    offSet = 0

    def __init__(self, cryptModule, key):
        self.cryptModule = cryptModule(key)
        self.key = key

    def encrypt(self, plainText):

        n = len(plainText)

        self.offSet = self.offSet + n

        bin = self.cryptModule.generate(n)

        binText = [(char) for char in plainText]

        cipherText = []

        for (rand, text) in zip(bin, binText):
            cipherText.append(rand ^ text)

        return bytes(cipherText)

    def decrypt(self, cipherText):

        n = len(cipherText)

        self.offSet = self.offSet + n

        bin = self.cryptModule.generate(n)

        binText = [(char) for char in cipherText]

        plainText = []

        for (rand, text) in zip(bin, binText):
            plainText.append(rand ^ text)

        return bytes(plainText)

    def encryptWO(self, plainText, offset):

        n = len(plainText)

        self.offSet = offset + n

        bin = self.cryptModule.getInterval(offset, offset + n)

        binText = [(char) for char in plainText]

        cipherText = []

        for (rand, text) in zip(bin, binText):
            cipherText.append(rand ^ text)

        cipherText = bytes(cipherText)

        return cipherText

    def decryptWO(self, cipherText, offset):

        n = len(cipherText)

        self.offSet = offset + n

        bin = self.cryptModule.getInterval(offset, offset + n)

        binText = [(char) for char in cipherText]

        plainText = []

        for (rand, text) in zip(bin, binText):
            plainText.append(rand ^ text)

        plainText = bytes(plainText)

        return plainText

    def getOffset(self):
        return self.offSet
