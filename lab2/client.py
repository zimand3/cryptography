# Ziman David zdim1981 524/2
import socket
import threading
from byteCrypt import byteCrypt
from solitaire import Solitaire

HOST = '127.0.0.1'
PORT = 5050
server = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
server.connect((HOST, PORT))
SIZE = 1024
with open("deck.txt") as f:
    deck = f.read().split('\n')
for i in range(len(deck)):
    deck[i] = int(deck[i])
crypting = byteCrypt(Solitaire, deck)


def clRecv(socket):
    offset = int(socket.recv(SIZE).decode())
    print("Recieved offset:", offset)
    resp = socket.recv(SIZE)
    print("Text recieved:")
    if (crypting.getOffset() != int(offset)):
        plainText = crypting.decryptWO(resp, offset)
    else:
        plainText = crypting.decrypt(resp)
    while resp != 'exit':
        print(plainText.decode())
        resp = socket.recv(SIZE).decode()


def clSend(socket, user):
    while True:
        msgType = input()

        if msgType == "exit" or msgType == "x":
            socket.send(msgType.encode())
            rcv = socket.recv(SIZE).decode()
            print(rcv)
            break

        if msgType == "online" or msgType == "o":
            socket.send(msgType.encode())

        if msgType == "public" or msgType == "p":
            content = input('Type in your message: ')
            byText = bytes(content, 'ascii')
            offset = str(crypting.getOffset())
            cipherText = crypting.encrypt(byText)
            socket.send(msgType.encode())
            print("Sent offset:", offset)
            socket.send(offset.encode())
            print("Encoded text:", cipherText)
            socket.send(cipherText)


userName = input("Username: ")
server.send(userName.encode())
print('Actions:\n\t(p)ublic -> send message to all users!\n\t(o)nline -> see who is online!\n\te(x)it -> exit.')

inThread = threading.Thread(target=clRecv, args=(server,))
inThread.start()

outThread = threading.Thread(target=clSend, args=(server, userName))
outThread.start()
inThread.join()
outThread.join()
server.close()
