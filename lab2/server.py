# Ziman David zdim1981 524/2
import socket
import threading


server = socket.socket(socket.AF_INET, socket.SOCK_STREAM)

HOST = '127.0.0.1'
PORT = 5050
SIZE = 1024
server.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
server.bind((HOST, PORT))
server.listen(100)
connections = []
userNames = []


def clThread(conn, addr):
    user = conn.recv(SIZE).decode()
    connections.append(conn)
    userNames.append(user)
    print(user + ' has logged in!')

    while True:
        msgType = conn.recv(SIZE).decode()
        if msgType == 'exit' or msgType == "x":
            conn.send('Logging out!'.encode())
            if user in userNames:
                userNames.remove(user)
            if conn in connections:
                connections.remove(conn)
            print(user + ' logged out!')
            break

        if msgType == 'online' or msgType == "o":
            msg = ''
            for i in userNames:
                msg += '<' + i + '>' + '\n'
            conn.send(msg.encode())

        if msgType == 'public' or msgType == "p":
            offset = conn.recv(SIZE)
            print(offset)
            content = conn.recv(SIZE)
            print(content)
            print("Ciphertext recieved: ", content)
            for cn in connections:
                if cn != conn:
                    cn.send(offset)
                    cn.send(content)


print("Server running!")

while True:
    conn, addr = server.accept()
    thr = threading.Thread(target=clThread, args=(conn, addr))
    thr.start()
server.close()
