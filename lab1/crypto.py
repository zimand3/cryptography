#!/usr/bin/env python3 -tt
"""
File: crypto.py
---------------
Assignment 1: Cryptography
Course: CS 41
Name: <Ziman David>
SUNet: <zdim1981>

Replace this with a description of the program.
"""
#import utils
import math
import string
# Caesar Cipher

def encrypt_caesar(plaintext):
    """Encrypt plaintext using a Caesar cipher.

    Add more implementation details here.
    """

    if not plaintext.isupper():
        raise ValueError

    res = ""

    for i in range(len(plaintext)):
        if plaintext[i] in string.ascii_uppercase:
            res += chr((ord(plaintext[i]) + 3 - 65) % 26 + 65)
        else:
            res += plaintext[i]

    return res
    #raise NotImplementedError  # Your implementation here


def decrypt_caesar(ciphertext):
    """Decrypt a ciphertext using a Caesar cipher.

    Add more implementation details here.
    """
    if not ciphertext.isupper():
        raise ValueError

    res = ""
    for i in range(len(ciphertext)):
        if ciphertext[i] in string.ascii_uppercase:
            res += chr((ord(ciphertext[i]) - 3  + 65) % 26 + 65)
        else:
            res += ciphertext[i]

    return res

    #raise NotImplementedError  # Your implementation here


# Vigenere Cipher

def encrypt_vigenere(plaintext, keyword):
    """Encrypt plaintext using a Vigenere cipher with a keyword.

    Add more implementation details here.
    """

    if not plaintext.isupper() or not keyword.isupper():
        raise ValueError

    res = ""
    for i in range(len(plaintext)):
        if plaintext[i] in string.ascii_uppercase:
            shift = ord(keyword[i%len(keyword)]) - 65
            res += chr((ord(plaintext[i]) + shift  - 65) % 26 + 65)
        else:
            res += plaintext[i]

    return res
    #raise NotImplementedError  # Your implementation here


def decrypt_vigenere(ciphertext, keyword):
    """Decrypt ciphertext using a Vigenere cipher with a keyword.

    Add more implementation details here.
    """
    if not ciphertext.isupper() or not keyword.isupper():
        raise ValueError

    res = ""
    for i in range(len(ciphertext)):
        if ciphertext[i] in string.ascii_uppercase:
            shift = ord(keyword[i%len(keyword)]) - 65
            res += chr((ord(ciphertext[i]) - shift  + 65) % 26 + 65)   
        else:
            res += ciphertext[i]

    return res
    #raise NotImplementedError  # Your implementation here

#Scytale Cipher
def encrypt_scytale(plaintext, circumference):
    if not plaintext.isupper() or not circumference > 0:
        raise ValueError

    res = [''] * circumference

    for i in range(len(plaintext)):
        res[i % circumference] += plaintext[i]

    return ''.join(res)

def decrypt_scytale(ciphertext, circumference):
    
    if not ciphertext.isupper() or not circumference > 0:
        raise ValueError

    decrypt_circ = math.ceil(len(ciphertext) / circumference)
    full_rows = len(ciphertext) % circumference
    incomp_rows = (decrypt_circ * circumference) - len(ciphertext)

    res = [''] * decrypt_circ
        
    col = 0
    row = 0

    for char in ciphertext:
        res[col] += char
        col += 1

        if (col == decrypt_circ) or (col == decrypt_circ - 1 and row >= circumference - incomp_rows):
            col = 0
            row += 1

    return ''.join(res)

#Railfence cipher

def encrypt_railfence(plaintext, rails):
    if rails < 1 or not plaintext.isupper():
        raise ValueError

    rail_list = ['']*rails
    goin_up = False
    counter = 0

    for i in range(len(plaintext)):
        if not goin_up:
            rail_list[counter] += (plaintext[i])
            if counter % rails == rails-1:
                goin_up = True
                counter = counter - 1
            else:
                counter = counter + 1
        else:
            rail_list[counter] += (plaintext[i])
            if counter % rails == 0:
                goin_up = False
                counter = counter + 1
            else:
                    counter = counter - 1
    return(''.join(rail_list))

def decrypt_railfence(cipher, rails):
    rail = [['' for i in range(len(cipher))] for j in range(rails)]
    goin_down = None
    row, col = 0, 0

    for i in range(len(cipher)):
        if row == 0:
            goin_down = True
        else:
            if row == rails - 1:
                goin_down = False

        rail[row][col] = '*'
        col += 1
        if goin_down:
            row += 1
        else:
            row -= 1
    index = 0

    for i in range(rails):
        for j in range(len(cipher)):
            if ((rail[i][j] == '*') and (index < len(cipher))):
                rail[i][j] = cipher[index]
                index += 1
    result = []
    row, col = 0, 0
    for i in range(len(cipher)):
        if row == 0:
            goin_down = True
        else:
            if row == rails-1:
                goin_down = False
        if (rail[row][col] != '*'):
            result.append(rail[row][col])
            col += 1
        if goin_down:
            row += 1
        else:
            row -= 1

    return(''.join(result))

# Merkle-Hellman Knapsack Cryptosystem

def generate_private_key(n=8):
    """Generate a private key for use in the Merkle-Hellman Knapsack Cryptosystem.



    Following the instructions in the handout, construct the private key components
    of the MH Cryptosystem. This consistutes 3 tasks:

    1. Build a superincreasing sequence `w` of length n
        (Note: you can check if a sequence is superincreasing with `utils.is_superincreasing(seq)`)
    2. Choose some integer `q` greater than the sum of all elements in `w`
    3. Discover an integer `r` between 2 and q that is coprime to `q` (you can use utils.coprime)

    You'll need to use the random module for this function, which has been imported already

    Somehow, you'll have to return all of these values out of this function! Can we do that in Python?!

    @param n bitsize of message to send (default 8)
    @type n int

    @return 3-tuple `(w, q, r)`, with `w` a n-tuple, and q and r ints.
    """
    raise NotImplementedError  # Your implementation here

def create_public_key(private_key):
    """Create a public key corresponding to the given private key.

    To accomplish this, you only need to build and return `beta` as described in the handout.

        beta = (b_1, b_2, ..., b_n) where b_i = r × w_i mod q

    Hint: this can be written in one line using a list comprehension

    @param private_key The private key
    @type private_key 3-tuple `(w, q, r)`, with `w` a n-tuple, and q and r ints.

    @return n-tuple public key
    """
    raise NotImplementedError  # Your implementation here


def encrypt_mh(message, public_key):
    """Encrypt an outgoing message using a public key.

    1. Separate the message into chunks the size of the public key (in our case, fixed at 8)
    2. For each byte, determine the 8 bits (the `a_i`s) using `utils.byte_to_bits`
    3. Encrypt the 8 message bits by computing
         c = sum of a_i * b_i for i = 1 to n
    4. Return a list of the encrypted ciphertexts for each chunk in the message

    Hint: think about using `zip` at some point

    @param message The message to be encrypted
    @type message bytes
    @param public_key The public key of the desired recipient
    @type public_key n-tuple of ints

    @return list of ints representing encrypted bytes
    """
    raise NotImplementedError  # Your implementation here

def decrypt_mh(message, private_key):
    """Decrypt an incoming message using a private key

    1. Extract w, q, and r from the private key
    2. Compute s, the modular inverse of r mod q, using the
        Extended Euclidean algorithm (implemented at `utils.modinv(r, q)`)
    3. For each byte-sized chunk, compute
         c' = cs (mod q)
    4. Solve the superincreasing subset sum using c' and w to recover the original byte
    5. Reconsitite the encrypted bytes to get the original message back

    @param message Encrypted message chunks
    @type message list of ints
    @param private_key The private key of the recipient
    @type private_key 3-tuple of w, q, and r

    @return bytearray or str of decrypted characters
    """
    raise NotImplementedError  # Your implementation here

