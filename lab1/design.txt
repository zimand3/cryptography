Name: <ZIMAN DAVID>
SUNet: <zdim1981>

In 1-3 sentences per section, comment on your approach to each of the parts of the assignment. What was your high-level strategy? How did you translate that into code? Did you make use of any Pythonic practices? We want you to reflect on your coding style, and whether you're making full use of the utilities provides.


# Caesar Cipher
Shifting letters by ascii code, then converting the ascii code to characters.

# Vigenere Cipher
Just like caesar, but the shift here is the index of the actual character in the abc from the keyword

# Scytale Cipher
In this one i used a list of strings, and i appended each character to the list to its row, then i merge the rows

# Railfence Cipher
The railfence cipher works similar to the Scytale cipher, but here i have a counter that shows where should the actual character be inserted. This counter moves from up to down.